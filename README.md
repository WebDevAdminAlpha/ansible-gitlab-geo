### This project is deprecated in favor of the [GitLab Environment Toolkit](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit). 

---

## Introduction

This repository includes an ansible playbook that make it easier to spin
up GitLab Geo.

The goal is to spin up two nodes of GitLab Geo: primary and secondary.

#### bootstrap.yml

This Ansible playbook does the following:

1. Creates all the nodes in Google Cloud
2. Registers both nodes in DNS (created A record for each node)
3. Installs GitLab EE on both servers
4. Installs GitLab License
5. Starts database replication
6. Enables hashed storage
7. Disables writes to authorized_keys file
8. Copies secrets and SSH keys
9. Installs PostgreSQL certificate for SSL
10. Creates firewall rules for both nodes so they are accessible using 80 ports

# Configuration

## SSH keys for GCP project

This playbook assumes that you have a pair of keys in `~/.ssh/ansible` and `~/.ssh/ansible.pub`.
To generate it, run:

```shell
ssh-keygen -t rsa -b 4096 -C "<your-name>-ansible" -f ~/.ssh/ansible
```

Copy the public key to your clipboard:

```shell
cat ~/.ssh/ansible.pub | pbcopy
```

NOTE :if you use a different filename than `~/.ssh/ansible`, please ensure
you update `private_key_file` in `ansible.cfg`.

Add this key to GCP project Compute Engine ⇒ Metadata ⇒ SSH Keys tab.
Pick a unique Username, e.g. `<your-name>-ansible`.

## GCP service account

Create a service account in "IAM & admin ⇒ Service Account" and grant "Compute Admin" and "DNS Administrator" permissions for it.
Create JSON key file and download it, you will use it later.

```
gcloud iam service-accounts keys create gcp-account.json --iam-account <your-name>-ansible@group-geo-f9c951.iam.gserviceaccount.com

gcloud projects add-iam-policy-binding group-geo-f9c951 \
--member serviceAccount:<your-name>-ansible@group-geo-f9c951.iam.gserviceaccount.com \
--role=roles/compute.admin

gcloud projects add-iam-policy-binding group-geo-f9c951 \
--member serviceAccount:<your-name>-ansible@group-geo-f9c951.iam.gserviceaccount.com \
--role=roles/dns.admin
```

## Project configuration

Copy `vars/settings.yml.example` to `vars/settings.yml` and configure it.

## Choosing a GitLab version

The ansible playbook supports running either a specific GitLab EE version, or the latest GitLab ee package available in the selected repository.
You can configure the behaviour in `vars/settings.yml` using a combination of `gitlab_repositry`, `gitlab_version`, and `install_state` values.

This configuration example uses GitLab EE version `11.11.5`:

```yml
gitlab_repository: gitlab-ee
gitlab_version: gitlab-ee=11.11.5-ee.0
install_state: present
```
and this example uses the latest GitLab EE released version (at the time of writing `12.0.3`):
```yml
gitlab_repository: gitlab-ee
gitlab_version: gitlab-ee
install_state: latest
```
For more detailed information regarding ansible and apt, please have a look at the [apt module documentation](https://docs.ansible.com/ansible/latest/modules/apt_module.html)

# Usage

## Bootstrapping / creating

Run:

```shell
ansible-playbook bootstrap.yml -e "prefix_name=<prefix> gitlab_root_password=<password>" -u <ssh-username>
```

Where:

- `<prefix>` is used to name the nodes and to create subdomains in DNS
  zone. The prefix should be unique in the GCP project. If you need to
  set up Geo nodes with the same prefix, just remove them first
  manually and re-run this playbook. Later, it's possible to automate
  the removal if needed.
- `<ssh-username>` (optional) is the username you filled when
  configuring [SSH keys for GCP project](#ssh-keys-for-gcp-project).
- `<password>` (optional) is the desired password for the initial GitLab root
  user. If not defined, it will be randomized and printed.

## Tearing down / removing

Run:

```shell
ansible-playbook teardown.yml -e "prefix_name=<prefix>"
```

Where:

- `<prefix>` is the same as used for bootstrapping / creating.

# Troubleshooting

Try adding `-vvvv` to your `ansible` command line to increase the
level of information printed.

# Dependencies

Ansible can be installed in many ways. If you want to run it natively on your local machine,
install with [Pipenv](https://docs.pipenv.org/en/latest/):

```shell
pipenv install
```

To access the virtualenv where ansible is installed:

```shell
pipenv shell
```

It works for all the platforms (MacOS, Linux, Windows).

In MacOS you can install Pipenv with:

```shell
brew install pipenv
```

If you want to have it inside a docker container, you can build and run from our Makefile:

```shell
make docker-build
make docker-run
```

# TODO

Check out the [open issues](https://gitlab.com/gitlab-org/geo-team/ansible-gitlab-geo/issues).
