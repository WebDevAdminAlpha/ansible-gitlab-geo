FROM ubuntu:20.04

RUN apt update -y && apt install -y python3-pip openssh-client
RUN pip3 install ansible requests google-auth apache-libcloud

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
WORKDIR '/ansible'

CMD ["/bin/bash"]
